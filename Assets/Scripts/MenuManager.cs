﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Gabrielle Hollaender 100623554
public class MenuManager : MonoBehaviour
{
    public void LoadApplication()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadUserProfile()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }

}
