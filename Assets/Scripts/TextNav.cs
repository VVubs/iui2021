﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextNav : MonoBehaviour
{
    public Text page1;
    public Text page2;

    public void ChangePage()
    {
        if (page1.gameObject.activeInHierarchy)
        {
            page1.gameObject.SetActive(false);
            page2.gameObject.SetActive(true);
        }

        else
            Debug.Log("Not active");
    }
}
