﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ClickBias : MonoBehaviour
{

    public List<Collider2D> cols;
    public TextMeshProUGUI displayText;
    public TextMeshProUGUI targetText;

    [SerializeField, Range(0f, 500f)]
    private float _clickTolerance = 0f; // This is in pixels
    private Vector3 _clickPos; // Last click position recorded
    private Vector2 _closest; // Vector for storing closest point to 
    private Collider2D _clickedCollider; // Collider that was last clicked on directly
    private Collider2D _hitCollider; // Collider that was last clicked on via its click bias
    private int _hitMissRecord = 0;
    private LineRenderer _line;

    [SerializeField]
    private List<BiasedCollider> _colliderBiases;


    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            _clickPos = Input.mousePosition;
            NewProcessClick(_clickPos);

            for(int i = 0; i < _colliderBiases.Count; i++)
            {
                _colliderBiases[i].UpdateTextIndicator();
            }
        }

    }

    private void NewProcessClick(Vector3 mousePos)
    {
        // Loop through colliders and see if any were hit
        _closest = Vector2.zero;
        _clickedCollider = null;

        for(int i = 0; i < _colliderBiases.Count; i++)
        {
            _closest = _colliderBiases[i].GetCol().ClosestPoint(mousePos);

            _colliderBiases[i].SetDistToClick(Vector3.Distance(_closest, mousePos));

            if (Vector3.Distance(_closest, mousePos) <= 0) // User clicked directly on a collider
            {
                _clickedCollider = _colliderBiases[i].GetCol();
            }
        }

        if(_clickedCollider != null)
        {
            Debug.Log("We hit a collider. Collider hit:" + _clickedCollider);
            _hitMissRecord++;
        }

        else
        {
            //Debug.Log("We hit nothing.");
            // Check biases of colliders
            CheckColliderBiases(mousePos);
        }
    }

    void CheckColliderBiases(Vector3 mousePos)
    {
        // Same variables as above
        _closest = Vector2.zero;
        _hitCollider = null;

        // Check each struct
        for(int i = 0; i < _colliderBiases.Count; i++)
        {
            _closest = _colliderBiases[i].GetCol().ClosestPoint(mousePos);

            _colliderBiases[i].SetDistToClick(Vector3.Distance(_closest, mousePos));

            Debug.Log(_colliderBiases[i].GetDistToClick());

            if(Vector3.Distance(_closest, mousePos) <= _colliderBiases[i].GetClickBias()) // If the mouse is clicked within the collider bias value
            {
                _hitCollider = _colliderBiases[i].GetCol();
            }
        }

        if(_hitCollider != null)
        {
            Debug.Log("We hit a collider's click bias. Collider hit: " + _hitCollider);
            Button temp;
            if (_hitCollider.gameObject.TryGetComponent<Button>(out temp))
            {
                temp.onClick.Invoke();
            }
            _hitMissRecord++;
        }

        else
        {
            _hitMissRecord--;
            BiasedCollider closeCol = new BiasedCollider(null, 0);

            float lowDist = int.MaxValue;

            // Expand and contract biases
            foreach (BiasedCollider collider in _colliderBiases)
            {

                if(collider.GetDistToClick() < lowDist)
                {
                    lowDist = collider.GetDistToClick();
                    closeCol = collider;
                }
            }

            CalculateNewBias(closeCol);
        }
    }

    void CalculateNewBias(BiasedCollider recalc)
    {
        Debug.Log("Recalculating..." + recalc.GetCol().name);

        float newBias;

        // New bias = previous bias + (previous bias - distance from collider) + collider size x value / 10 + collider size y value / 10 + 5 * hit miss record
        newBias = recalc.GetClickBias() + /* (recalc.GetClickBias() + recalc.GetDistToClick()) */ + (recalc.GetCol().bounds.size.x / 10) + (recalc.GetCol().bounds.size.y / 10) + 5 * (-_hitMissRecord);

        recalc.SetClickBias(newBias);

        Debug.Log(newBias);
    }

    private void Start()
    {
        _colliderBiases = new List<BiasedCollider>();

        // Register colliders in key value pair list (set click bias as 0 for all colliders)
        foreach(Collider2D col in cols)
        {
            _colliderBiases.Add(new BiasedCollider(col, 20));
        }

        for(int i = 0; i < _colliderBiases.Count; i++)
        {
            //m_textIndicator = m_collider.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            _colliderBiases[i].SetTextIndicator(_colliderBiases[i].GetCol().transform.GetChild(0).GetComponent<TextMeshProUGUI>());
            _colliderBiases[i].UpdateTextIndicator();
        }

        _line = GetComponent<LineRenderer>();
        _line.useWorldSpace = false;
    }

}
