﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

//Gabrielle Hollaender 100623554
public class UserProfile : MonoBehaviour
{
    //UI objects
    public InputField usernameField;
    public Dropdown bias;
    public Dropdown consent;
    public Dropdown disability;

    //text objects
    public Text twoA_yes;
    public Text twoA_no;
    public Text consentText;

    public GameObject nextScene;
    //public GameObject quitScene;

    private string username;

    void Start()
    {
        //lets record when they start answering the questions
        Directory.CreateDirectory(Application.streamingAssetsPath + "/User_Profile_Logs/");
        
        //CreateUserProfile();
    }

    // Update is called once per frame
    void Update()
    {
        //save the user name
        username = usernameField.text;
       

        //change bias question depending on response
        if (disability.value == 1) //if the answer is No
        {
            twoA_no.gameObject.SetActive(true);
            twoA_yes.gameObject.SetActive(false);

            bias.gameObject.SetActive(true);
           
        }

        if (disability.value == 2) //if the answer is Yes
        {
            twoA_yes.gameObject.SetActive(true);
            twoA_no.gameObject.SetActive(false);

            bias.gameObject.SetActive(true);
           
        }

        //what ever option they select, consent is active
        if (bias.value == 1) // they don't want bias
        {
            consentText.gameObject.SetActive(true);
            consent.gameObject.SetActive(true);

            
        }

        if (bias.value == 2) //they want bias
        {
            //give em the last question
            consentText.gameObject.SetActive(true);
            consent.gameObject.SetActive(true);

            
        }

        if (consent.value == 1) //no consent
        {
            nextScene.gameObject.SetActive(true);
        }

        if (consent.value == 2) //yes consent
        {
            nextScene.gameObject.SetActive(true);
        }
    }

    //cute little piece of code that checks consent before saving vital information, if they say no it will only save their name.
    public void CreateUserProfile()
    {
        if(usernameField.text == "")
        {
            return;
        }

        //file path
        string userProf = Application.streamingAssetsPath + "/User_Profile_Logs/" + "UserProfile.txt";
        //create file is it doesn't exist
        if (!File.Exists(userProf))
        {
            File.WriteAllText(userProf, "User Profile Information \n\n");
            File.AppendAllText(userProf, "Time of Login: " + System.DateTime.Now + "\n");
        }
        
        File.AppendAllText(userProf, "Username: " + usernameField.text + "\n");

        if (consent.value == 0) //if the answer is NA
            return;

        if (consent.value == 1) //no consent
        {
            File.AppendAllText(userProf, "Consent Denied \n");
        }

        if (consent.value == 2) //yes consent
            {
                File.AppendAllText(userProf, "Consent Approved \n");
            
            if (disability.value == 0) //if the answer is NA
                return;

            else if (disability.value == 1) //if the answer is No
            {
                File.AppendAllText(userProf, "No Disability \n");
            }

            else if (disability.value == 2) //if the answer is Yes
            {
                File.AppendAllText(userProf, "Disability Present \n");
            }


            if (bias.value == 0) //if the answer is NA
                return;

            else if (bias.value == 1) // they don't want bias
            {
                File.AppendAllText(userProf, "No Bias \n");
            }

            else if (bias.value == 2) //they want bias
            {
                File.AppendAllText(userProf, "Bias Needed \n");
            }

        }

    }
}

