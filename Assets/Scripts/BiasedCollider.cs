﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BiasedCollider : MonoBehaviour
{

    private Collider2D m_collider;
    public TextMeshProUGUI m_textIndicator;
    private float m_clickBias;
    private float m_distToClick;

    public BiasedCollider(Collider2D col, float bias)
    {
        m_collider = col;
        m_clickBias = bias;
    }

    public void SetCol(Collider2D col)
    {
        m_collider = col;
    }

    public void SetClickBias(float bias)
    {
        m_clickBias = bias;
    }

    public void SetDistToClick(float click)
    {
        m_distToClick = click;
    }

    public void SetTextIndicator(TextMeshProUGUI text)
    {
        m_textIndicator = text;
    }

    public void UpdateTextIndicator()
    {
        m_textIndicator.text = m_clickBias.ToString();
    }

    public float GetDistToClick()
    {
        return m_distToClick;
    }

    public float GetClickBias()
    {
        return m_clickBias;
    }

    public Collider2D GetCol()
    {
        return m_collider;
    }

}
